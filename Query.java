// Generated by Haxe 4.1.5
package hgltf;

import haxe.root.*;

@SuppressWarnings(value={"rawtypes", "unchecked"})
public class Query extends haxe.lang.HxObject
{
	public Query(haxe.lang.EmptyObject empty)
	{
	}
	
	
	public Query()
	{
		//line 7 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		hgltf.Query.__hx_ctor_hgltf_Query(this);
	}
	
	
	protected static void __hx_ctor_hgltf_Query(hgltf.Query __hx_this)
	{
	}
	
	
	public java.lang.Object parse(java.lang.String str)
	{
		//line 11 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		boolean copyAll = ( haxe.lang.Runtime.valEq(haxe.lang.StringExt.substr(str, 0, 1), "-") || haxe.lang.Runtime.valEq(str, "") );
		//line 12 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.EReg isOr = new haxe.root.EReg("or", "");
		//line 13 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.EReg isProp = new haxe.root.EReg(".*:[><=!]?", "");
		//line 14 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.EReg isName = new haxe.root.EReg("[^:/]", "");
		//line 15 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.EReg isExclude = new haxe.root.EReg("^-", "");
		//line 17 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		java.lang.Object[] q = new java.lang.Object[]{new haxe.lang.DynamicObject(new java.lang.String[]{}, new java.lang.Object[]{}, new java.lang.String[]{}, new double[]{})};
		//line 18 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.Array<java.lang.String> token = haxe.lang.StringExt.split(str, " ");
		//line 19 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.root.Array ors = new haxe.root.Array();
		//line 20 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		ors.push(q[0]);
		//line 22 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		haxe.lang.Function[] match = new haxe.lang.Function[]{null};
		//line 22 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		match[0] = new hgltf.Query_parse_22__Fun(q, match, isProp, isName, isExclude);
		//line 49 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		{
			//line 49 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			int _g = 0;
			//line 49 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			int _g1 = token.length;
			//line 49 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			while (( _g < _g1 ))
			{
				//line 49 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				int i = _g++;
				//line 50 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				if (isOr.match(token.__get(i))) 
				{
					//line 51 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
					q[0] = new haxe.lang.DynamicObject(new java.lang.String[]{}, new java.lang.Object[]{}, new java.lang.String[]{}, new double[]{});
					//line 52 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
					ors.push(q[0]);
				}
				else
				{
					//line 53 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
					match[0].__hx_invoke2_o(0.0, token.__get(i), 0.0, null);
				}
				
			}
			
		}
		
		//line 55 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		return new haxe.lang.DynamicObject(new java.lang.String[]{"copy_all", "or"}, new java.lang.Object[]{copyAll, ors}, new java.lang.String[]{}, new double[]{});
	}
	
	
	@Override public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties)
	{
		//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		{
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			boolean __temp_executeDef1 = true;
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			if (( field != null )) 
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				switch (field.hashCode())
				{
					case 106437299:
					{
						//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
						if (field.equals("parse")) 
						{
							//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
							__temp_executeDef1 = false;
							//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
							return ((haxe.lang.Function) (new haxe.lang.Closure(this, "parse")) );
						}
						
						//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
						break;
					}
					
					
				}
				
			}
			
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			if (__temp_executeDef1) 
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
			}
			else
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				throw null;
			}
			
		}
		
	}
	
	
	@Override public java.lang.Object __hx_invokeField(java.lang.String field, java.lang.Object[] dynargs)
	{
		//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
		{
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			boolean __temp_executeDef1 = true;
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			if (( field != null )) 
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				switch (field.hashCode())
				{
					case 106437299:
					{
						//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
						if (field.equals("parse")) 
						{
							//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
							__temp_executeDef1 = false;
							//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
							return this.parse(haxe.lang.Runtime.toString(dynargs[0]));
						}
						
						//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
						break;
					}
					
					
				}
				
			}
			
			//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
			if (__temp_executeDef1) 
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				return super.__hx_invokeField(field, dynargs);
			}
			else
			{
				//line 5 "/mnt/c/Users/Leon/Desktop/projects/hgltf/src/_code/queries/src/hgltf/Query.hx"
				throw null;
			}
			
		}
		
	}
	
	
}


