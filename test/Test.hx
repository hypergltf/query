class Text {

  static public function main():Void {
  	trace( parse("foo or bar") );
  	trace( parse("class:fopoer or bar foo:bar").or[0] );
  	trace( parse("-skybox class:foo").or[0] );
  	trace( parse("foo/flop moo or bar").or[0] );
  	trace( parse("-foo/flop moo or bar").or[0] );
  	trace( parse("price:>4 moo or bar").or[0] );
  	trace( parse("price:>=4 moo or bar").or[0] );
  	trace( parse("price:<=4 moo or bar").or[0] );
  	trace( parse("price:!=4 moo or bar").or[0] );
  }
}
