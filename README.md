## Query

HGLTF query function in various languages

> NOTE: the source is in `src/hgltf/Query.hx`, run (and ./install) `haxe build.hxml`

<b>Status:</b>

* javascript: stable
* python: stable
* lua: untested 
* cpp: untested 
* cppia: untested
* java: untested
* cs: untested
