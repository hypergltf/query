package hgltf;

@:expose  // <- makes the class reachable from plain JavaScript
@:keep    // <- avoids accidental removal by dead code elimination
class Query {

  public function new()  {}

  public function parse(str:String) : Dynamic {

	var copyAll:Bool       = str.substr(0,1) == "-" || str == "";
	var isOr:EReg          = ~/or/;
	var isProp:EReg        = ~/.*:[><=!]?/;
	var isName:EReg        = ~/[^:\/]/;
	var isExclude:EReg     = ~/^-/;

	var q:haxe.DynamicAccess<Dynamic> = {};
  	var token = str.split(" ");
	var ors   = new Array();
	q.set("object", new Array() );
	q.set("-object", new Array() );
	ors.push(q);

  	function match(str,prefix = ""){
	  if( isExclude.match(str) ){
	  	var t = str.substr(1);
		match(t,"-");
		return;
	  }
	  if( isProp.match(str) ){
		var skip = 0;
		var type = "=";
	  	if( str.indexOf(">") != -1  ) type = ">";
	  	if( str.indexOf("<") != -1  ) type = "<";
	  	if( str.indexOf("!=") != -1 ) type = "!=";
	  	if( str.indexOf(">=") != -1 ) type = ">=";
	  	if( str.indexOf("<=") != -1 ) type = "<=";
		if( type != "=" ) skip += type.length;
		var property = str.split(":")[0];
		var value:haxe.DynamicAccess<Dynamic> = {}
		value.set(type, str.split(":")[1].substr(skip) );
		q.set(prefix+property,value);
		return;
	  }
	  if( isName.match(str) ){
		if( prefix == '-' ) q["-object"].push(str);
		else q["object"].push(str);
		return;
	  }
	}

	for( i in 0...token.length ) {
	  if( isOr.match(token[i]) ){ 
		q = {};
	  	ors.push(q);
	  }else match(token[i]);
    }
	return { or: ors, copy_all: copyAll }
  }
}
